#include <Wire.h>
 // slave

#define endereco 0x08 //endereco para o master referenciar o slave
int t=0;
#define ledVis 13
#define sensorVis A1
#define sensortemperatura A0
bool estado=true;
byte comando=0;
void setup ()
{
    Wire.begin(endereco); 
    Wire.onReceive(escolhas); //funcao que escolhe o comando a ser realizado com base no botao que o usuario apertou
    Wire.onRequest(funcoes); //funcao que realiza o comando com base no valor lido na funcao "escolhas"
    Serial.begin(9600);
    analogReference(DEFAULT);  
    pinMode(ledVis,OUTPUT);
    pinMode(sensorVis,INPUT);
    pinMode(sensortemperatura,INPUT);
}

void loop()
{
   if(estado==true) //vê se os dados chegaram
   {
     estado=false; //avisa que a resposta foi enviada
   }
}

void escolhas(int t )
{
  comando=Wire.read();  //lê o comando enviado pelo master
}

void funcoes() // funcao que seta a funcao com base no valor lido em "comando=Wire.read()"
{   
        if(comando == 1)
        {
            digitalWrite(ledVis,HIGH);
            int calibradoinfravermelho=0;
            delay(1000);
            calibradoinfravermelho=analogRead(sensorVis);
            delay(1000);
            Wire.write(calibradoinfravermelho); //envia um valor analogico que será convertido no master para ser usado em calculo como uma referencia
            digitalWrite(ledVis,LOW);
        }
        if(comando == 2)
        {
            digitalWrite(ledVis,HIGH);
            int amostraVis=0; 
            amostraVis=analogRead(sensorVis);  //lê o sensor no infravermelho
            Wire.write(amostraVis); //envia um valor analogico da amostra já calibrada
            digitalWrite(ledVis,LOW);
        }
        if(comando == 3)
        {
            int temp=0;
            delay(1000); 
            temp=analogRead(sensortemperatura); //lê sensor de temeperatura
            delay(1000);
            Wire.write(temp); //envia valor analogico lido do sensor para ser convertido no master 
        }
}
