#include <Wire.h>
 // slave

#define endereco 0x08 //endereco para o master referenciar o slave
int t=0;
#define ledIR 13
#define sensorIR A2
#define sensortemperatura A0
bool estado=true;
byte comando;
void setup ()
{
    Wire.begin(endereco); 
    Wire.onReceive(escolhas); //funcao que escolhe o comando a ser realizado com base no botao que o usuario apertou
    Wire.onRequest(funcoes); //funcao que realiza o comando com base no valor lido na funcao "escolhas"
    Serial.begin(9600);
    analogReference(INTERNAL); //usa a referencia interna do arduino de 1.1 volts para poder diminuir o "degrau" de leitura 
    pinMode(ledIR,OUTPUT);
}

void loop()
{
   if(estado==true) //vê se os dados chegaram
   {
     estado=false; //avisa que a resposta foi enviada
   }
}

void escolhas(int t )
{
  comando=Wire.read();  //lê o comando enviado pelo master
}
void funcoes() // funcao que seta a funcao com base no valor lido em "comando=Wire.read()"
{   
        if(comando == 1)
        {
          calibrainfravermelho();
        }
        if(comando == 2)
        {
           infravermelho();
        }
        if(comando == 3)
        {
           temperatura();
        }
}

void calibrainfravermelho() //funcao que retorna um valor analogico de calibração para ser utilizado de referencia em analises no infravermelho
{
     digitalWrite(ledIR,HIGH);
     int calibradoinfravermelho=analogRead(sensorIR);
     delay(6000000);
     digitalWrite(ledIR,LOW);
     Wire.write(calibradoinfravermelho); //envia um valor analogico que será convertido no master para ser usado em calculo como uma referencia
}

 void infravermelho()
 {
     calibrainfravermelho();
     digitalWrite(ledIR,HIGH);
     int amostrainfravermelho=analogRead(sensorIR);  //lê o sensor no infravermelho
     delay(6000000);
     digitalWrite(ledIR,LOW);
     Wire.write(amostrainfravermelho); //envia um valor analogico da amostra já calibrada
 }

void temperatura()
{
    int temp=analogRead(sensortemperatura); //lê sensor de temeperatura
    Wire.write(temp); //envia valor analogico lido do sensor para ser convertido no master  
}
