#include <Wire.h>
 // slave

 #define endereco 0x08 //endereco para o master referenciar o slave
int ledIR=2;
int LEDvisivel=4;
#define TSL12=A1;
#define sensorIR A3
#define sensortemperatura A0
bool estado=false;
byte comando;
void setup ()
{
    Wire.begin(endereco); 
    Wire.onReceive(escolhas); //funcao que escolhe o comando a ser realizado com base no botao que o usuario apertou
    Wire.onRequest(funcoes); //funcao que realiza o comando com base no valor lido na funcao "escolhas"
    Serial.begin(9600);
    analogReference(INTERNAL); //usa a referencia interna do arduino de 1.1 volts para poder diminuir o "degrau" de leitura 
    pinMode(ledIR,OUTPUT);
    pinMode(LEDvisivel,OUTPUT);
}

void loop()
{
   if(estado==true) //vê se os dados chegaram
   {
     estado=false; //avisa que a resposta foi enviada
   }
}

void escolhas()
{
  comando=Wire.read();  //lê o comando enviado pelo master
}
void funcoes() // funcao que seta a funcao com base no valor lido em "comando=Wire.read()"
{
   switch(comando)
     {
        case 1:
        calibrarvisivel();
        break;
        case 2:
        visivel();
        break;
        case 3:
        infravermelho();
        break;
        case 4: 
        temperatura();
        break;
        case 5:
        calibrainfravermelho();
        break;
        default:
        break;
     }
}

void calibrarvisivel() //funcao para enviar um valor de referencia que é utilizado em calculos da analise no visivel
{
    digitalWrite(LEDvisivel,HIGH);
    int amostracalibrada=analogRead(TSL12);  //lê o sensor do visivel 
    Wire.write(amostracalibrada); //envia um valor analogico de calibração para o master
    digitalWrite(LEDvisivel,LOW);
}

void visivel()
{
     digitalWrite(LEDvisivel,HIGH);
     int amostravisivel=analogRead(TSL12); 
    Wire.write(amostravisivel); //envia um valor analogico da amostra já calibrada
    digitalWrite(LED,LOW);         
}
 void infravermelho()
 {
     calibrainfravermelho();
     digitalWrite(ledIR,HIGH);
     int amostrainfravermelho=analogRead(sensorIR);  //lê o sensor no infravermelho
     Wire.write(amostrainfravermelho); //envia um valor analogico da amostra já calibrada
     digitalWrite(ledIR,LOW);
 }

void temperatura()
{
    int temp=analogRead(sensortemperatura); //lê sensor de temeperatura
    Wire.write(temp); //envia valor analogico lido do sensor para ser convertido no master  
}

void calibrainfravermelho() //funcao que retorna um valor analogico de calibração para ser utilizado de referencia em analises no infravermelho
{
     digitalWrite(ledIR,HIGH);
     int calibradoinfravermelho=analogRead(sensorIR);
     Wire.write(calibradoinfravermelho); //envia um valor analogico que será convertido no master para ser usado em calculo como uma referencia
     digitalWrite(ledIR,LOW);
}
