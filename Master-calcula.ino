#include <math.h>
#include <LiquidCrystal.h>
#include <Wire.h>

//master//com4
#define endereco 0x08 //endereco para refenciar o slave
#define botao1 8 //define as entradas que lerao o teclado
#define botao2 9
#define botao3 13
#define botao4 10
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
volatile float calibradovisivel=0; //valor a ser usado como referencia nos calculos
volatile float calibradoinfravermelho=0;
int contvis=0, contIR=0, conttemp=0;
#define conversao 0.48828125 //fator de conversao a ser utilizado para converter um valor analogico em uma grandeza fisica ou quimica
void setup()
{
    Wire.begin();
    Serial.begin(9600);
    lcd.begin(16, 2);
    pinMode(botao1, INPUT_PULLUP);
    pinMode(botao2, INPUT_PULLUP);
    pinMode(botao3, INPUT_PULLUP);
    pinMode(botao4, INPUT_PULLUP);  
}
 
void loop()
{
     int botao1pressionado=digitalRead(botao1); //realiza a leitura constantemente do botoes do teclado
     int botao2pressionado=digitalRead(botao2);
     int botao3pressionado=digitalRead(botao3);
     int botao4pressionado=digitalRead(botao4);
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Aguardando...");
     delay(1000);
     if(!botao1pressionado) //funcao a ser realizada quando o botao 1 é pressionado
     {
          contvis=0; //zera o contador que contem a quantidade de vezes em que a funcao botao2 é lida
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Calibrando vis");
          delay(3000);
          byte comando=1; //envia o valor 1 para o slave que diz a respeito da funcao que retorna um valor calibrado para analises no visivel
          Wire.beginTransmission(endereco); //inicia uma transmissao com o slave referenciado pela variavel endereco
          Wire.write(comando); //envia o valor do comando a ser realizado pelo slave
          button1(); //chama a funcao button1 que vai tratar o valor analogico enviado pelo slave
          Wire.endTransmission(); //encerra a transmissao com o slave
     }
     if(!botao2pressionado) 
     {
        contvis++; //incrementa o contvis para saber quantas vezes a funcao foi acessado
        if(contvis==4) //contvis é usado para saber quando chamar a funcao de calibrar de novo, pois o usuario só poderá um mesmo valor calibrado apenas 3 vezes
        {   //esse if é para recalibrar novamente o valor de referencia, já que o usuario só pode utilizar um mesmo valor calibrado tres vezes
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Aperte 1");
          lcd.setCursor(0,1);
          lcd.print("para recalibrar");
          delay(3000);
        }
        if(contvis<4) //condicao para  realizar as analises no visivel, essa condicao menor do que 4 diz a respeito que a pessoa só pode utilizar o valor calibrado tres vezes
        {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Analise no vis");
          delay(3000);
          byte comando=2; 
          Wire.beginTransmission(endereco);
          Wire.write(comando); //envia 2 para o slave que irá chamar a funcao que lê dados utilizando o ledvisivel e o sensor do visivel
          button2(); //chama a funcao que irá tratar os dados sobre analogicos sobre a analise no visivel
          Wire.endTransmission();
        }
     }
     if(!botao3pressionado)
     {  //contIR é um contador para comandar o botao3, levando em consideração que o usuario sempre fará analises no visivel, quando ele apertar o botao 3 que diz respeito ao infravermelho
        contIR++; //o contador servirá para indicar quando deve ser realizado uma calibração ou quando deve ser realizado a analise no infravermelho
        if(contIR==1)
        { //quando o usuario aperta para o botao 3 pela primeira vez essa condicao é satisfeita, pois sempre deve se obter um valor de calibração antes de comecar a analise
          // essa funcao também serve para descartar o primeiro valor
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Calibrando");
          lcd.setCursor(0,1);
          lcd.print("Aperte 3");
          delay(3000); 
        }
        if(contIR==2)
        {
          byte comando=5; 
          calibraIR(comando); //chama a funcao calibra IR para obter um valor calibrado para analises no infravermelho
        }
        if(contIR==6)
        { //se o mesmo valor de calibração tiver sido usado 3 vezes, entao a funcao irá reiniciar o contador que cairá na primeira condicao e o valor de referencia ser calibrado novamente
          contIR=0;
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Aperte 3");
          lcd.setCursor(0,1);
          lcd.print("para recalibrar");
          delay(3000);
        }
        if(contIR>2 && contIR<6)
        { //condicao que serve para fazer as analises no IR propriamente dito quando o valor de calibração não foi usado mais de 3 vezes
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Analise no IR");
          delay(3000);
          byte comando=3;
          Wire.beginTransmission(endereco);
          Wire.write(comando); //envia 3 para o slave que irá chamar a funcao que retorna valores analogicos de analises no infravermelho
          button3(); //chama a funcao que irá tratar os dados analogicos enviados pelo slave 
          Wire.endTransmission();
        }
     }
     if(!botao4pressionado) 
     {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Calculando...");
          delay(3000);
          byte comando=4;
          Wire.beginTransmission(endereco);
          Wire.write(comando); //envia 4 para o slave que irá chamar a funcao que retorna valores analogicos da temperatura
          button4(); //chama a funcao que trata o valor analogico da temperatura
          Wire.endTransmission();
     }
}

void button1() //funcao que trata dos dados analogicos a respeito da calibração para analises no visivel
{
     Wire.requestFrom(endereco,4); //requisita 4 bytes de informacao (uma varivavel do tipo inteiro) do slave que é referenciado pela variavel endereco
     int calibrado=Wire.read(); //lê o valor analogico enviado pelo slave
     calibradovisivel=(((1.1)*calibrado)/(1023)); //formula para converter o valor analogico em um valor real 
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Vis calibrado");
     lcd.setCursor(0,1);
     lcd.print(calibradovisivel);
     delay(5000);
}

void button4() //funcao que trata dos dados analogicos a respeito da temperatura
{
          Wire.requestFrom(endereco,4);
          int valoranalogico=Wire.read();
          float temp=(((valoranalogico*(1.1))/(1023))/0.01); 
          if(temp==0)
          { //descarta valores de temperatura que sejam iguais a zero e tenta reler o valor do sensor
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("Aperte 4");
            lcd.setCursor(0,1);
            lcd.print("novamente");
            delay(3000);
          }
          if(temp>0)
          { //mostra os valores de temperatura caso esses sejam maior que 0
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("Temperatura");
            lcd.setCursor(0,1);
            lcd.print(temp);
            lcd.setCursor(5,1);
            lcd.print("C");
            delay(5000);
          }
}

void button2() //funcao para tratar os dados da analise do visivel
{
        Wire.requestFrom(endereco,4);
        int amostra=Wire.read();
        float amostraconvertida=(((1.1)*amostra)/(1023)); 
        float transmitancia=(amostraconvertida/calibradovisivel); //utiliza o valor calibrado pela funcao button 1 para achar uma grandeza quimica
        float absorbancia= -log(transmitancia); //calcula de fato a grandeza que o equipamento tem o trabalho de quantificar
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Transmitancia vis");
        lcd.setCursor(0,1);
        lcd.print(transmitancia);
        delay(5000);
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Absorbancia vis");
        lcd.setCursor(0,1);
        lcd.print(absorbancia);
        delay(5000);
}

void button3() //funcao que trata dos valores a respeito dos dados das analises no infravermelho e realiza basicamente os mesmos calculos que a funcao acima
{
     Wire.requestFrom(endereco,4);
     int amostrainfravermelho=Wire.read();
     float amostraconvertidair=(((1.1)*amostrainfravermelho)/(1023));
     float transmitanciair=(amostraconvertidair/calibradoinfravermelho);
     float absorbanciair=-log(transmitanciair);
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Transmitancia IR");
     lcd.setCursor(0,1);
     lcd.print(transmitanciair);
     delay(5000);
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("Absorbancia IR");
     lcd.setCursor(0,1);
     lcd.print(absorbanciair);
     delay(5000);
}

void calibraIR(byte chamacalibra) // funcao que trata a forma como será calibrada a amostra de referencia para analises no infravermelho
{ 
        int cont=0;
        cont++; //contador para setar o que a funcao deve fazer com base em quantas vezes ela foi acessada
        if(cont==1)
        { //se for a primeira que a funcao está sendo acessada, printa um aviso e chama a funcao que calibra propriamente o  valor de referencia
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Calibrando IR");
          delay(3000);
          Wire.beginTransmission(endereco);
          Wire.write(chamacalibra); // envia o valor 5 para o slave que irá retornar um valor analogico da amostra calibrada que será usada nas analises IR
          buttonespecial(); //chama a funcao que irá converter os dados analogicos para o valor de calibração
          Wire.endTransmission();
        }
        if(cont>1)
        { //se a funcao já foi acessada mais de uma vez não é necessario printar um aviso e apenas  a funcao de tratamento é chamada
          Wire.beginTransmission(endereco);
          Wire.write(chamacalibra);
          buttonespecial();
          Wire.endTransmission();
        }
}

void buttonespecial() //chama a funcao que irá converter os dados analogicos para um valor de referencia calibrado para analises no IR
{
     Wire.requestFrom(endereco,4);
     int calibrado=Wire.read();
     calibradoinfravermelho=(((1.1)*calibrado)/(1023));
     if(calibradoinfravermelho==0)
     { //nao mostra o valor calibrado caso esse seja igual a 0, pois não há valores nulos para calibração, pede-se para precisar o botao 3 afim de realizar nova tentativa de calibração
       conttemp=0;
       lcd.clear();
       lcd.setCursor(0,0);
       lcd.print("Aperte 3");
       lcd.setCursor(0,1);
       lcd.print("novamente");
       delay(3000);
     }
     if(calibradoinfravermelho>0)
     { //mostra o valor calibrado caso esse maior que 0
       lcd.clear();
       lcd.setCursor(0,0);
       lcd.print("IR calibrado");
       lcd.setCursor(0,1);
       lcd.print(calibradoinfravermelho); 
       delay(5000);
     }
}
